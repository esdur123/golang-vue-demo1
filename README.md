# golang-vue-demo1

Windows系統

將golang和vue串在一起的專案練習

0.請先安裝go+設定環境變數 (參考網頁)
    https://oranwind.org/go-go-yu-yan-yu-windows-shang-zhi-an-zhuang-yu-huan-jing-she-ding/

1.安裝node.js

2.重開機

3.安裝vue-cli
  npm install -g vue-cli
  
4.開一個你自行命名的資料夾aaa

    在aaa裡面創一個vue專cli專案 我命名為vue(所以最後一個單字是vue)
  
    vue init webpack vue
  
    (他會問一堆 基本上前面四個都Enter, Install vue-router選Yes, 其他我都No, 有一個選NPM)

5.選到你剛創的專案
  cd vue 

6.npm run build

以上是vue

7.接下來需要建一個main.go在這個vue資料夾的外面
>     package main
>     
>     import (
>     	"github.com/gin-gonic/gin"
>     )
>     
>     func main() {
>     	r := gin.Default()
>     	r.LoadHTMLGlob("vue/dist/*.html")              // 添加入口index.html
>     	r.LoadHTMLFiles("vue/static/*/*")              // 添加资源路径
>     	r.Static("/static", "vue/dist/static")         // 添加资源路径
>     	r.StaticFile("/hello/", "vue/dist/index.html") //前端接口
>     
>     	r.Run(":8080")
>     }


8.用vscode打開aaa資料夾 (請先在Extensions裝好支援go)

9.開一個Terminal執行 go run main.go

10.如果沒有自動下載gin-gonic/gin, 請打go get github.com/gin-gonic/gin

    然後執行 go run main.go 或直接按F5
   
    等他一下 應該會有防火牆選項跳出來 讓他通過
   
11.這時候瀏覽器 http://localhost:8080/hello/#/
